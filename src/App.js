import './App.css';
// import Home from './components/Home/Home';
// import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';

function App() {
  return (
    <div className="App">
      {/* <Home /> */}
      {/* <Login /> */}
      <Signup />
    </div>
  );
}

export default App;
