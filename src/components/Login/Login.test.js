import { render } from '@testing-library/react';
import Login from './Login';

test('renders Login', () => {
  render(<Login />);
});

test('Login - "Log in" Header', () => {
  render(<Login />);
  const loginHeader = document.querySelector('.ut-login-header');
  expect(loginHeader).toBeInTheDocument();
});

test('Login - "Continue with Google" button', () => {
  render(<Login />);
  const continueWithGoogleBtn = 
    document.querySelector('.ut-continue-with-google-btn');
  expect(continueWithGoogleBtn).toBeInTheDocument();
});

test('Login - "Continue with Facebook" button', () => {
  render(<Login />);
  const continueWithFacebookBtn = 
    document.querySelector('.ut-continue-with-facebook-btn');
  expect(continueWithFacebookBtn).toBeInTheDocument();
});

test('Login - "Continue with Twitter" button', () => {
  render(<Login />);
  const continueWithTwitterBtn = 
    document.querySelector('.ut-continue-with-twitter-btn');
  expect(continueWithTwitterBtn).toBeInTheDocument();
});

test('Login - "Continue with GitHub" button', () => {
  render(<Login />);
  const continueWithGitHubBtn = 
    document.querySelector('.ut-continue-with-github-btn');
  expect(continueWithGitHubBtn).toBeInTheDocument();
});

test('Login - Email and Password Form', () => {
  render(<Login />);
  const loginEmailInput = document.querySelector('.ut-login-email-input');
  const loginPasswordInput = document.querySelector('.ut-login-password-input');
  const loginBtn = document.querySelector('.ut-login-log-in-btn');
  const loginKeepMeLoggedInCheckbox = 
    document.querySelector('.ut-keep-me-logged-in-checkbox');
  const loginForgotPasswordBtn = document.querySelector('.ut-forgot-password-btn');
  expect(loginEmailInput).toBeInTheDocument();
  expect(loginPasswordInput).toBeInTheDocument();
  expect(loginBtn).toBeInTheDocument();
  expect(loginKeepMeLoggedInCheckbox).toBeInTheDocument();
  expect(loginForgotPasswordBtn).toBeInTheDocument();
});

test('Login - Sign Up Reference', () => {
  render(<Login />);
  const loginSignUpBtn = document.querySelector('.ut-sign-up-btn');
  expect(loginSignUpBtn).toBeInTheDocument();
});