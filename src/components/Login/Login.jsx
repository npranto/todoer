import React from 'react'
import './Login.css';

export default function Login() {
  return (
    <div className="Login">
      <main>
        <div className="logo">
          <a href="#/">
            <svg height="32" width="33" xmlns="http://www.w3.org/2000/svg" className="_1HWbT"><mask id="td-logo_svg__a" fill="#fff"><path d="M0 0h32.042v32H0z" fillRule="evenodd"></path></mask><g fill="none" fillRule="evenodd"><path d="M4.005 0A4.014 4.014 0 000 4v24c0 2.2 1.802 4 4.005 4h24.032c2.203 0 4.005-1.8 4.005-4V4c0-2.2-1.802-4-4.005-4z" fill="#e44332" mask="url(#td-logo_svg__a)"></path><g fill="#fff"><path d="M6.792 15.157l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 9.287v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path><path d="M6.792 20.277l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 14.407v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path><path d="M6.792 25.717l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 19.847v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path></g></g></svg>
            {/* <img src="https://todoist.com/users/showlogin" alt="Logo" /> */}
          </a>
        </div>

        <h1 className="header ut-login-header">Log in</h1>

        <ul className="login-with-socials">
          <li>
            <button type="button" className="login-with-google ut-continue-with-google-btn">
              <div className="social-logo">
                <img src="/assets/img/google.png" alt="Google" />
              </div>
              <span>Continue with Google</span>
            </button>
          </li>
          <li>
            <button type="button" className="login-with-facebook ut-continue-with-facebook-btn">
              <div className="social-logo">
                <img src="/assets/img/facebook.png" alt="Facebook" />
              </div>
              <span>Continue with Facebook</span>
            </button>
          </li>
          <li>
            <button type="button" className="login-with-twitter ut-continue-with-twitter-btn">
              <div className="social-logo">
                <img src="/assets/img/twitter.png" alt="Twitter" />
              </div>
              <span>Continue with Twitter</span>
            </button>
          </li>
          <li>
            <button type="button" className="login-with-github ut-continue-with-github-btn">
              <div className="social-logo">
                <img src="/assets/img/github.png" alt="Github" />
              </div>
              <span>Continue with Github</span>
            </button>
          </li>
        </ul>

        <div className="divider">
          <hr className="line" />
          {/* <div className="line"></div> */}
          <div className="content"> OR </div>
          <hr className="line" />
          {/* <div className="line"></div> */}
        </div>

        <div className="login-email-password">
          <form className="login-email-password-form">
            <div className="email-field field">
              <label htmlFor="email-input">Email</label>
              <input 
                type="email" 
                className="ut-login-email-input" 
                id="email-input" 
              />
            </div>
            <div className="password-field field">
              <label htmlFor="password-input">Password</label>
              <input 
                type="password" 
                className="ut-login-password-input" 
                id="password-input" />
            </div>
            <div className="login-actions">
              <div className="log-in-btn-block">
                <button type="button" className="log-in-btn ut-login-log-in-btn">
                  Log in
                </button>
              </div>
              <div className="keep-me-logged-in-block">
                <input 
                  type="checkbox" 
                  className="ut-keep-me-logged-in-checkbox" 
                  id="keep-me-logged-in-checkbox" 
                  name="keep-me-logged-in-checkbox" />
                <label htmlFor="keep-me-logged-in-checkbox"> Keep me logged in</label>
              </div>
              <div className="forgot-password-block">
                <a href="#/" className="forgot-password-btn ut-forgot-password-btn">Forgot your password?</a>
              </div>
            </div>
            <div></div>
          </form>
        </div>

        <div className="divider">
          <hr />
        </div>

        <div className="references">
          <div className="sign-up-ref-block">
            <p>
              Don't have an account? <a href="#/" className="sign-up-btn ut-sign-up-btn">Sign up</a>
            </p>
          </div>  
        </div>

      </main>
    </div>
  )
}
