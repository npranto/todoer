import { render, screen } from '@testing-library/react';
import Home from './Home';

test('renders Home', () => {
  render(<Home />);
});

test('renders Home - Nav', () => {
  render(<Home />);
  const signUpElement = screen.getByText(/Sign up/i);
  const logInElement = screen.getByText(/Log in/i);
  const menuIconElement = document.querySelector('.ut-menu-icon');
  const logoSVGElement = document.querySelector('.logo svg');
  expect(signUpElement).toBeInTheDocument();
  expect(logInElement).toBeInTheDocument();
  expect(menuIconElement).toBeInTheDocument();
  expect(logoSVGElement).toBeInTheDocument();
});

test('renders Home - Intro Jumbotron', () => {
  render(<Home />);
  const titleElement = screen.getByText(/Organize it all with Todoer/i);
  const getStartedBtnElement = document.querySelector('.intro-jumbotron .get-started-btn');
  const clipArtCartoonElement = document.querySelector('.clip-art-cartoon');
  const backgroundStarsElement = document.querySelector('.background-stars');
  const tdElement = document.querySelector('.todoer-dashboard');
  const tdmElement = document.querySelector('.todoer-dashboard-mobile');  
  expect(titleElement).toBeInTheDocument();
  expect(getStartedBtnElement).toBeInTheDocument();
  expect(clipArtCartoonElement).toBeInTheDocument();
  expect(backgroundStarsElement).toBeInTheDocument();
  expect(tdElement).toBeInTheDocument();
  expect(tdmElement).toBeInTheDocument();
});

test('renders Home - Free Mental Space', () => {
  render(<Home />);
  const titleElement = screen.getByText(/Free up your mental space/i);
  const descElement = screen.getByText(/Regain clarity and calmness by getting all those tasks out of your head and onto your to-do list \(no matter where you are or what device you use\)\./)

  const btfElement = document.querySelector('.free-mental-space .ut-browse-todoer-features-link');
  expect(titleElement).toBeInTheDocument();
  expect(descElement).toBeInTheDocument();
  expect(btfElement).toBeInTheDocument();
});

test('renders Home - Achieve Peace', () => {
  render(<Home />);
  const titleElement = screen.getByText(/Achieve peace of mind with Todoist/i);
  const getStartedBtnElement = document.querySelector('.achieve-peace .get-started-btn');

  expect(titleElement).toBeInTheDocument();
  expect(getStartedBtnElement).toBeInTheDocument();
});

test('renders Home - Footer', () => {
  render(<Home />);
  const descElement = screen.getByText(/Join millions of people who organize work and life with Todoist./i);
  const darkLogoElement = document.querySelector('footer .left-side .logo-dark');
  const twitterIconElement = document.querySelector('footer .twitter');
  const youtubeIconElement = document.querySelector('footer .youtube');
  const facebookIconElement = document.querySelector('footer .facebook');
  const instagramIconElement = document.querySelector('footer .instagram');

  expect(descElement).toBeInTheDocument();
  expect(darkLogoElement).toBeInTheDocument();
  expect(twitterIconElement).toBeInTheDocument();
  expect(youtubeIconElement).toBeInTheDocument();
  expect(facebookIconElement).toBeInTheDocument();
  expect(instagramIconElement).toBeInTheDocument();
});
