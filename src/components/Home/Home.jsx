import './Home.css';

function Home() {
  return (
    <div className="Home">
      <header>
        {/* nav */}
        <nav>
          <ul className="nav-list">
            <li className="nav-item stretch">
              <button type="button" className="logo">
                <svg height="32" width="33" xmlns="http://www.w3.org/2000/svg" className="_1HWbT"><mask id="td-logo_svg__a" fill="#fff"><path d="M0 0h32.042v32H0z" fillRule="evenodd"></path></mask><g fill="none" fillRule="evenodd"><path d="M4.005 0A4.014 4.014 0 000 4v24c0 2.2 1.802 4 4.005 4h24.032c2.203 0 4.005-1.8 4.005-4V4c0-2.2-1.802-4-4.005-4z" fill="#e44332" mask="url(#td-logo_svg__a)"></path><g fill="#fff"><path d="M6.792 15.157l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 9.287v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path><path d="M6.792 20.277l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 14.407v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path><path d="M6.792 25.717l12.865-7.479c.275-.16.289-.653-.02-.83-.308-.178-.893-.514-1.111-.643a1.004 1.004 0 00-.991.012c-.154.09-10.433 6.06-10.776 6.256-.412.236-.92.24-1.33 0L0 19.847v2.708c1.321.778 4.607 2.71 5.403 3.165.475.27.93.264 1.389-.003"></path></g></g></svg>
              </button>
            </li>
            <li className="nav-item">
              <a href="#/" className="sign-up-btn">
                Sign up
              </a>
            </li>
            <li className="nav-item">
              <a href="#/" className="log-in-btn">
                Log in
              </a>
            </li>
            <li className="nav-item">
              <button type="button" className="menu-icon">
                <img src="assets/img/menu-icon.png" alt="Menu Icon" className="ut-menu-icon" />
              </button>
            </li>
          </ul>
        </nav>
      </header>

      <main>
        <article>
          {/* intro-jumbotron */}
          <section className="block intro-jumbotron">
            <h1 className="title">Organize it all with Todoer</h1>
            <div className="call-to-action">
              <button type="button" className="get-started-btn">Get Started</button>
            </div>
            <div className="jumbotron-cover">
              <img src="https://todoist.com/_next/static/images/header@2x_b52d8f7c7bf19d6c702569d1072ed6a2.webp" alt="Clip Art Cartoon" className="clip-art-cartoon" />

              <img src="https://todoist.com/_next/static/images/header-bg@2x_d49daad6e384274c19f96f6aad65a615.webp" alt="Background Stars" className="background-stars" />

              <img src="https://todoist.com/_next/static/images/screenshot@2x_44c1cf78bc12457546d889573e04345a.webp" alt="Todoer Dashboard" className="todoer-dashboard" />

              <img src="https://todoist.com/_next/static/images/screenshot-phone@2x_e35aacb2dec92f091fce33a2ba8e99b5.webp" alt="Todoer Dashboard Mobile" className="todoer-dashboard-mobile" />
            </div>
          </section>
        </article>

        {/* free-mental-space */}
        <section className="block free-mental-space">
          <h2 className="title">Free up your mental space</h2>
          <p className="description">Regain clarity and calmness by getting all those tasks out of your head and onto your to-do list (no matter where you are or what device you use).
          </p>
          <div className="browse-todoer-features">
            <a href="#/" className="ut-browse-todoer-features-link">
              <span className="play-icon">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" width="24" height="24"><g fillRule="evenodd" clipRule="evenodd"><path d="M12 3a9 9 0 100 18 9 9 0 000-18zM4.929 4.929A10 10 0 1119.07 19.07 10 10 0 014.93 4.93z"></path><path d="M10.008 9.051a.042.042 0 00-.008.026v5.846c0 .011.003.018.008.026a.11.11 0 00.042.034.152.152 0 00.15-.005l4.76-2.923a.09.09 0 00.034-.032.046.046 0 000-.046.09.09 0 00-.034-.032L10.2 9.022a.152.152 0 00-.15-.005.11.11 0 00-.043.034zM9 9.077c0-.911 1.031-1.331 1.722-.907l4.761 2.923c.69.423.69 1.391 0 1.814l-4.76 2.923c-.692.424-1.723.005-1.723-.907V9.077z"></path></g></svg>
              </span>
              <span>Browse Todoist’s features</span>
            </a>
          </div>
        </section>

        {/* achieve-peace */}
        <section className="block achieve-peace">
          <h2 className="title">Achieve peace of mind with Todoist</h2>
          <div className="call-to-action">
            <button type="button" className="get-started-btn">
              Get Started
            </button>
          </div>

        </section>
      </main>

      {/* footer */}
      <footer>
        <div className="left-side">
          <a href="#/" className="logo-dark">
            <svg fill="none" height="32" width="32" xmlns="http://www.w3.org/2000/svg"><clipPath id="td-monochrome_svg__a"><path d="M0 0h32v32H0z"></path></clipPath><g clipPath="url(#td-monochrome_svg__a)"><path clipRule="evenodd" d="M4 0h24c2.2 0 4 1.8 4 4v24c0 2.2-1.8 4-4 4H4c-2.2 0-4-1.8-4-4v-5.374l.057.033c1.365.795 4.592 2.674 5.382 3.12.478.27.936.264 1.397-.002.276-.16 3.302-1.9 6.36-3.66l.015-.01.06-.034 6.515-3.75c.277-.16.291-.65-.019-.827l-.218-.124-.001-.001c-.317-.18-.726-.413-.9-.516a1.02 1.02 0 00-.998.012 23821 23821 0 01-10.847 6.235 1.349 1.349 0 01-1.338 0L0 19.927v-2.693l.057.033c1.365.795 4.592 2.674 5.382 3.12.478.27.936.264 1.397-.002.276-.16 3.307-1.903 6.367-3.664l.024-.014.019-.011.01-.006 6.53-3.758c.277-.16.291-.651-.019-.828l-.218-.124c-.316-.18-.726-.414-.901-.517a1.02 1.02 0 00-.998.012A23821 23821 0 016.803 17.71a1.349 1.349 0 01-1.338 0L0 14.536v-2.693l.056.032c1.365.795 4.592 2.674 5.383 3.12.478.27.936.264 1.397-.002l6.374-3.668.008-.005c3.2-1.84 6.426-3.698 6.568-3.78.277-.16.291-.65-.019-.828l-.218-.124c-.317-.18-.727-.414-.901-.516a1.02 1.02 0 00-.998.012c-.154.089-10.5 6.038-10.847 6.235a1.349 1.349 0 01-1.338 0L0 9.143V4c0-2.2 1.8-4 4-4z" fill="#202020" fillRule="evenodd"></path></g></svg>
          </a>
          <p>Join millions of people who organize work and life with Todoist.</p>
          <ul className="social-list">
            <li className="social-item twitter">
              <a href="#/">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true"><path d="M22.393 3.339a8.874 8.874 0 01-2.868 1.121A4.452 4.452 0 0016.23 3c-2.49 0-4.512 2.072-4.512 4.628 0 .363.039.715.116 1.054-3.75-.193-7.076-2.034-9.304-4.837a4.711 4.711 0 00-.61 2.329c0 1.605.796 3.022 2.008 3.852a4.424 4.424 0 01-2.046-.577v.056c0 2.244 1.556 4.115 3.622 4.539a4.305 4.305 0 01-1.19.162c-.29 0-.574-.027-.849-.081.575 1.838 2.24 3.177 4.216 3.212A8.91 8.91 0 011 19.256a12.564 12.564 0 006.919 2.077c8.303 0 12.842-7.05 12.842-13.167 0-.202-.004-.403-.011-.6A9.269 9.269 0 0023 5.17a8.84 8.84 0 01-2.592.729 4.621 4.621 0 001.985-2.56z"></path></svg>
              </a>
            </li>
            <li className="social-item youtube">
              <a href="#/">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true"><path d="M3.598 4.242C6.958 4 11.995 4 11.995 4h.01s5.038 0 8.396.242l.155.016c.515.05 1.427.138 2.25.995.72.725.954 2.371.954 2.371S24 9.557 24 11.49v1.812c0 1.933-.24 3.866-.24 3.866s-.234 1.646-.954 2.371c-.823.858-1.735.946-2.25.996-.057.005-.108.01-.155.016-3.358.241-8.401.249-8.401.249s-6.24-.057-8.16-.24c-.091-.017-.202-.03-.327-.045-.609-.073-1.562-.187-2.32-.976-.719-.725-.953-2.37-.953-2.37S0 15.234 0 13.301V11.49c0-1.933.24-3.866.24-3.866s.234-1.646.954-2.37c.823-.858 1.735-.947 2.25-.996.057-.006.108-.01.154-.016zm12.408 7.912L9.521 8.787l.001 6.711 6.484-3.344z" fillRule="evenodd" clipRule="evenodd"></path></svg>
              </a>
            </li>
            <li className="social-item facebook">
              <a href="#/">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true"><path d="M23 12.067C23 5.955 18.075 1 12 1S1 5.955 1 12.067C1 17.591 5.023 22.17 10.281 23v-7.734H7.488v-3.199h2.793V9.63c0-2.774 1.643-4.306 4.155-4.306 1.203 0 2.462.216 2.462.216v2.724h-1.387c-1.366 0-1.792.853-1.792 1.73v2.074h3.05l-.487 3.2h-2.563V23C18.977 22.17 23 17.591 23 12.067z"></path></svg>
              </a>
            </li>
            <li className="social-item instagram" >
              <a href="#/">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true"><path fillRule="evenodd" d="M6.608 12a5.392 5.392 0 1110.784 0 5.392 5.392 0 01-10.784 0zM12 15.5a3.5 3.5 0 110-7 3.5 3.5 0 010 7z" clipRule="evenodd"></path><path d="M17.605 7.655a1.26 1.26 0 100-2.52 1.26 1.26 0 000 2.52z"></path><path fillRule="evenodd" d="M12 1.5c-2.852 0-3.21.012-4.33.063-1.117.051-1.88.229-2.548.488a5.146 5.146 0 00-1.86 1.211 5.146 5.146 0 00-1.21 1.86c-.26.668-.438 1.431-.489 2.549C1.513 8.79 1.5 9.148 1.5 12c0 2.852.012 3.21.063 4.33.051 1.117.229 1.88.488 2.548.269.69.628 1.276 1.211 1.86.584.583 1.17.942 1.86 1.21.668.26 1.431.438 2.549.489 1.12.05 1.477.063 4.329.063 2.852 0 3.21-.012 4.33-.063 1.117-.051 1.88-.229 2.548-.488a5.149 5.149 0 001.86-1.211 5.149 5.149 0 001.21-1.86c.26-.668.438-1.431.489-2.549.05-1.12.063-1.477.063-4.329 0-2.852-.012-3.21-.063-4.33-.051-1.117-.229-1.88-.488-2.548a5.148 5.148 0 00-1.211-1.86 5.147 5.147 0 00-1.86-1.21c-.668-.26-1.431-.438-2.549-.489C15.21 1.513 14.852 1.5 12 1.5zm0 1.892c2.804 0 3.136.01 4.243.061 1.024.047 1.58.218 1.95.362.49.19.84.418 1.207.785.367.368.595.717.785 1.207.144.37.315.926.362 1.95.05 1.107.061 1.44.061 4.243 0 2.804-.01 3.136-.061 4.243-.047 1.024-.218 1.58-.362 1.95-.19.49-.418.84-.785 1.207a3.254 3.254 0 01-1.207.785c-.37.144-.926.315-1.95.362-1.107.05-1.44.061-4.243.061-2.804 0-3.136-.01-4.243-.061-1.024-.047-1.58-.218-1.95-.362-.49-.19-.84-.418-1.207-.785a3.253 3.253 0 01-.785-1.207c-.144-.37-.315-.926-.362-1.95-.05-1.107-.061-1.44-.061-4.243 0-2.804.01-3.136.061-4.243.047-1.024.218-1.58.362-1.95.19-.49.418-.84.785-1.207a3.253 3.253 0 011.207-.785c.37-.144.926-.315 1.95-.362 1.107-.05 1.44-.061 4.243-.061z" clipRule="evenodd"></path></svg>
              </a>
            </li>
          </ul>
        </div>
        <div className="right-side">
          <div className="terms-copyright">
            <div className="terms">
              <a href="#/">Security</a> | <span></span>
              <a href="#/">Privacy</a> | <span></span>
              <a href="#/">Terms</a>
            </div>
            <p>&copy; 2021 Todoer</p>
          </div>
        </div>
      </footer>
    </div>
  )
}

export default Home;