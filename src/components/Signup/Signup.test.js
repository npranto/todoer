import { render } from '@testing-library/react';
import Signup from './Signup';

test('renders Signup', () => {
  render(<Signup />);
});

test('Signup - "Sign up" Header', () => {
  render(<Signup />);
  const loginHeader = document.querySelector('.ut-signup-header');
  expect(loginHeader).toBeInTheDocument();
});

test('Signup - "Continue with Google" button', () => {
  render(<Signup />);
  const continueWithGoogleBtn = 
    document.querySelector('.ut-continue-with-google-btn');
  expect(continueWithGoogleBtn).toBeInTheDocument();
});

test('Signup - "Continue with Facebook" button', () => {
  render(<Signup />);
  const continueWithFacebookBtn = 
    document.querySelector('.ut-continue-with-facebook-btn');
  expect(continueWithFacebookBtn).toBeInTheDocument();
});

test('Signup - "Continue with Twitter" button', () => {
  render(<Signup />);
  const continueWithTwitterBtn = 
    document.querySelector('.ut-continue-with-twitter-btn');
  expect(continueWithTwitterBtn).toBeInTheDocument();
});

test('Signup - "Continue with GitHub" button', () => {
  render(<Signup />);
  const continueWithGitHubBtn = 
    document.querySelector('.ut-continue-with-github-btn');
  expect(continueWithGitHubBtn).toBeInTheDocument();
});

test('Signup - Name, Email and Password Form', () => {
  render(<Signup />);
  const signupNameInput = document.querySelector('.ut-signup-name-input');
  const signupEmailInput = document.querySelector('.ut-signup-email-input');
  const signupPasswordInput = document.querySelector('.ut-signup-password-input');
  const signupConfirmPasswordInput = document.querySelector('.ut-signup-confirm-password-input');
  const signupBtn = document.querySelector('.ut-signup-btn');
  const signupKeepMeLoggedInCheckbox = 
    document.querySelector('.ut-keep-me-logged-in-checkbox');
  
  expect(signupNameInput).toBeInTheDocument();
  expect(signupEmailInput).toBeInTheDocument();
  expect(signupPasswordInput).toBeInTheDocument();
  expect(signupConfirmPasswordInput).toBeInTheDocument();
  expect(signupBtn).toBeInTheDocument();
  expect(signupKeepMeLoggedInCheckbox).toBeInTheDocument();
});

test('Signup - Sign Up Reference', () => {
  render(<Signup />);
  const signupGoToLoginBtn = document.querySelector('.ut-go-to-login-btn');
  expect(signupGoToLoginBtn).toBeInTheDocument();
});