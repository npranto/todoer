# Todoer

A simple clone of Todoist application to track lists of tasks and/actions inside different groups or lists.

## Deployment

**Production**
[![Netlify Status](https://api.netlify.com/api/v1/badges/8b3b49ac-fd3f-4f56-9d28-489b9239bf9e/deploy-status)](https://app.netlify.com/sites/todoer-live/deploys)

**Development**
[![Netlify Status](https://api.netlify.com/api/v1/badges/5dd67748-d53d-44c0-b65b-5fa9077996e8/deploy-status)](https://app.netlify.com/sites/todoer-dev/deploys)

## Development

*Prerequisites*
- Node: v14+
- NPM: v6+
- git: v2+

*Clone*
```bash
git clone https://gitlab.com/npranto/todoer.git
```

*Install Dependencies*
```bash
cd todoer

npm install
```
*Kickoff Development Server*
```bash
npm start
```

## Feature / Bugfix Development Process

For new feature development or bugfix, here is the list of process to follow:
- create a new issue on GitLab [issue board](https://gitlab.com/npranto/todoer/-/issues) for a new feature or bugfix
- create a new branch off of `develop` branch and reference the issue number and title on the branch (i.e., "`25-create-a-documentation-for-developers-to-start-development-on-their-local-machine-2`")
- once feature or bugfix is complete:
  - add change description and update version on `CHANGELOG.md` file
  - update `package.json` and `npm-shrinkwrap.json` with new version
  - add new changes to stage
  - add commit 
  - push branch to remote repo
- now, on GitLab, create a pull request to merge feature or bugfix branch to `develop` branch
- verify that a new build has been kicked off and both `test` and `build` stages are passing 
- now, ask a core developer to review and approve the pull request
- once approved, merge the branch to `develop`  
- check Netlify (todoer-dev) automatic build [deploys](https://app.netlify.com/sites/todoer-dev/deploys) to ensure a new build has been kicked off and built successfully

## Release Process
  - create a new issue to release feature or bugfix to production on GitLab [issues board](https://gitlab.com/npranto/todoer/-/issues)
  - create a branch off of `develop` branch and prefix it w/ `release/<VERSION>`
  - update `CHANGELOG.md` file
    - locate the version to release to production (usually the latest version)
    - add `*(Released)*` flag next to version
  
    Before:
    ```
    ## [0.0.1] - 2021-11-17
    ```
    After:
    ```
    ## [0.0.1] - 2021-11-17 *(Released)*
    ```
  - push the release branch to remote repo
  - create a pull request to merge release branch to `master` branch
  - ask core developer to review pull request for approval
  - once approved, merge pull request
  - check Netlify (tododer-live) automatic build [deploys](https://app.netlify.com/sites/todoer-live/deploys) to ensure a new build has been kicked off and built successfully
  - create a new tag targeting the latest commit on `master` branch;  (i.e., tag name format - `v1.5.6`)

