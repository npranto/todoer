# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.9] - 2021-11-27
### Updated
- [#35](https://gitlab.com/npranto/todoer/-/issues/35) - Updates `npm-shrinkwrap.json` to fix NPM package security vulnerabilities

## [0.0.8] - 2021-11-24
### Added
- [#29](https://gitlab.com/npranto/todoer/-/issues/29) - Adds UI for sign up page

## [0.0.7] - 2021-11-24
### Added
- [#28](https://gitlab.com/npranto/todoer/-/issues/28) - Adds UI for log in page

## [0.0.6] - 2021-11-20
### Updated
- [#32](https://gitlab.com/npranto/todoer/-/issues/32) - Updates `.gitlab-ci.yml` by removing `build` stage as Netlify already auto triggers builds and keeps artifacts

## [0.0.5] - 2021-11-20
### Added
- [#27](https://gitlab.com/npranto/todoer/-/issues/27/) - Adds UI for `Home` page along with its corresponding unit tests

## [0.0.4] - 2021-11-19
### Added
- [#31](https://gitlab.com/npranto/todoer/-/issues/31)
  - Adds `.gitlab-ci-yml` to setup CI/CD pipeline
  - Now, pushing any branch to remote will auto trigger `test` and `build` stages to ensure code validation
  - Skips auto deploy in CI/CD step as Netlify automatically already triggers auto deploy when code is merged to either `develop` or `master` branch

## [0.0.3] - 2021-11-17
### Added
- [#26](https://gitlab.com/npranto/todoer/-/issues/26) - Added Ant Design UI framework for faster development

## [0.0.2] - 2021-11-17
### Added
- [#25](https://gitlab.com/npranto/todoer/-/issues/25) - Added CHANGELOG file
### Updated
- [#25](https://gitlab.com/npranto/todoer/-/issues/25)
  - Renames `package-lock.json` to `npm-shrinkwrap.json`
  - Updated README.md file with documentation for new feature/bugfix development work along with release process 

## [0.0.1] - 2021-11-10 *(Released)*
### Added
- Setup application w/ `create-react-app` CLI
